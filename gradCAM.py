import sys
import keras
import tensorflow as tf
from keras.models import Model
from keras import backend as K
from keras import activations
from keras.layers.convolutional import _Conv
from keras.layers.pooling import _Pooling1D, _Pooling2D, _Pooling3D
from keras.utils import plot_model

#from vis.visualization import visualize_saliency, visualize_cam
#from vis.visualization import visualize_activation
#from vis.utils import utils
from itertools import combinations

import matplotlib
matplotlib.use('Agg')
import pylab as pl
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches

import os
import numpy as np
import json
import pickle
import glob
import time
import random
import gc
from scipy.ndimage.interpolation import zoom
from scipy.ndimage import gaussian_filter

#Configuring GPU to be used
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
#config = tf.ConfigProto(gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.3))
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

#Defining a custom backpropagation rule for gradients in the network.
@tf.RegisterGradient('GuidedRelu')
def _guided_backprop(op, grad):
    dtype = op.outputs[0].dtype
    gate_g = tf.cast(grad > 0., dtype)
    gate_y = tf.cast(op.outputs[0] > 0, dtype)
    return gate_y * gate_g * grad  



def normalizeArr(array,minVal,maxVal,newMin, newMax):
    array = (array - minVal)/(maxVal - minVal + K.epsilon())
    array = (newMax - newMin) * array + newMin
    return array 

def compileComputeFn(this_model,this_layer):
    #Defines derivative of output with respect to input (in case of saliency or penultimate layer kernels in case of attention
    inp = this_model.input
    wrt = this_model.get_layer(this_layer).output
    out_layer = this_model.layers[-1]
    loss = K.mean(out_layer.output[:,0])
    grads = K.gradients(loss,wrt)[0]
    grads = grads / (K.sqrt(K.mean(K.square(grads))) + K.epsilon())
    iterate = K.function([inp],[grads,wrt])
    return iterate

#def getSaliency(xData,saliencyFn):
#    
#    grads = saliencyFn([xData])[0]
#    grads = np.reshape(grads,grads.shape[1:])
#    grads = np.abs(grads)
#    #grads = normalizeArr(grads,np.min(grads),np.max(grads),0,1)     
#    return grads

def getAttention(xData,attentionFn,Nseg=256,isRelu=1):
    
    vals = attentionFn([xData])
    grads = vals[0]
    grads = np.abs(grads)
    wrt = vals[1]
    print grads.shape
    print wrt.shape
    grads = np.reshape(grads,grads.shape[1:])
    wrt = np.reshape(wrt,wrt.shape[1:])


    att = np.zeros(wrt.shape[:2],dtype=np.float32)
    sum_grads = 0.0
    
    for j in xrange(wrt.shape[-1]):
        sum_grads += np.sum(grads[:,:,j])
    alpha = np.empty(wrt.shape[-1], dtype=np.float32)
    for j in xrange(wrt.shape[-1]):
        alpha[j] = np.sum(grads[:,:,j])/sum_grads        
    for j in xrange(wrt.shape[-1]):
        thisProd = wrt[:,:,j]*alpha[j]
        #thisProd *= posWeights[:,:,j]
        #att += wrt[:,:,j] * alpha[j]
        att += thisProd
        
    #if isRelu:
    #    att = att * (att > 0.0)
    #att = normalizeArr(att,np.min(att),np.max(att),0,1)     
    Nseg = float(Nseg)
    thisN = att.shape[0]
    att = zoom(att,(Nseg/thisN,Nseg/thisN))

    return att

K.set_learning_phase(0)

orgModel = keras.models.load_model('model_3.h5')

backpropedModel = keras.models.load_model('model_3.h5')
backpropedModel.layers[-1].activation = activations.linear
backpropedModel.save('model_3_temp.h5')
backpropedModel = keras.models.load_model('model_3_temp.h5')

attFn = compileComputeFn(backpropedModel,'concatenate_20')

files = glob.glob('../../shared/Data/HMI_LOS_SHARPS/valid_magnetograms/los/*.dat')
random.shuffle(files)

xData = np.load(files[0])
xData = xData.reshape(1,256,256,1)
attVals = getAttention(xData,attFn)
attVals.dump('testAttention.dat')
xData.dump('testData.dat')
