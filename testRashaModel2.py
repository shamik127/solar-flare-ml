from __future__ import print_function

import os.path
import glob

import keras
from keras import applications, metrics, layers, models, regularizers, optimizers
from keras.applications import ResNet50, Xception
from keras.applications.inception_v3 import InceptionV3
from keras.models import *
from keras.layers import *
from keras.callbacks import *
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import layer_utils, to_categorical
from keras.datasets import mnist

import numpy as np
import tensorflow as tf
import keras
import os
from keras import layers
from keras.layers import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D
from keras.models import Model, load_model
from keras.preprocessing import image
from keras.optimizers import Adam
from keras.utils import layer_utils, to_categorical
from keras.utils.data_utils import get_file
from keras.initializers import glorot_uniform
import scipy.misc

import psutil
from sklearn.metrics import confusion_matrix

import keras.backend as K
K.set_image_data_format('channels_last')
K.set_learning_phase(1)

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
config = tf.ConfigProto(gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5))
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

"""
trainRegions = ['1028', '1041', '1066', '115', '1256', '1350', '1449', '1461',
       '1464', '1582', '1603', '1621', '1722', '1724', '1750', '1834',
       '1879', '1946', '1996', '2040', '211', '2137', '2186', '2191',
       '2193', '2220', '2227', '2362', '245', '2693', '2739', '2748',
       '2760', '2790', '2878', '2887', '2920', '3048', '3056', '3258',
       '3263', '3291', '3295', '3321', '3341', '3364', '3366', '3376',
       '3437', '345', '3497', '3535', '3563', '3587', '362', '3686',
       '3688', '3721', '3740', '3766', '377', '3779', '3784', '3793',
       '3804', '3813', '3836', '3856', '3879', '394', '4000', '401',
       '407', '4071', '4097', '4138', '415', '4186', '4197', '4231',
       '4294', '437', '4639', '4698', '4781', '4874', '49', '4920', '495',
       '4955', '5011', '5026', '5107', '514', '5144', '5233', '5298',
       '54', '5415', '5446', '5447', '5526', '5637', '5653', '5673',
       '5885', '667', '746', '753', '814', '856', '878', '892', '1', '1001', '1019', '1021', '1026', '104', '1062', '1075', '1079',
       '1080', '1090', '1092', '1093', '1113', '1119', '1120', '1124',
       '114', '1149', '1186', '12', '1210', '1221', '1237', '1249',
       '1275', '1278', '128', '1300', '1303', '1309', '1312', '1313',
       '1318', '1338', '1339', '1342', '1348', '135', '1367', '1390',
       '1391', '1396', '1399', '1405', '1422', '1424', '1425', '1447',
       '1455', '1457', '1465', '1484', '1488', '1514', '1520', '1528',
       '156', '1573', '1578', '1596', '1628', '1632', '1634', '1642',
       '1644', '1653', '1662', '1677', '1688', '1705', '1711', '1715',
       '1727', '1795', '1819', '1832', '1863', '1866', '187', '1873',
       '1886', '1892', '1903', '1931', '1951', '1970', '1979', '1990',
       '2007', '2011', '2017', '2021', '2028', '2039', '2044', '2047',
       '2059', '2061', '2069', '2098', '2106', '2109', '2112', '2130',
       '218', '2181', '220', '224', '2240', '2245', '226', '2262', '2291',
       '2306', '2314', '2337', '2338', '2341', '2342', '2344', '2352',
       '2353', '2360', '2380', '2387', '241', '2411', '2414', '2433',
       '2450', '2460', '2469', '2489', '2492', '2501', '2504', '2511',
       '252', '2520', '2541', '2557', '256', '2571', '2573', '2581',
       '2583', '2585', '2587', '259', '2597', '26', '2625', '2634',
       '2651', '2661', '2672', '2677', '2685', '2691', '2696', '2718',
       '2727', '2732', '2733', '2737', '274', '2749', '2750', '2758',
       '2779', '279', '2822', '2832', '284', '2852', '2904', '2912',
       '2952', '2964', '2966', '2968', '297', '2981', '2984', '3012',
       '3019', '3022', '3031', '3032', '3049', '3066', '3068', '3082',
       '3097', '3098', '3114', '3115', '3119', '3122', '3129', '3149',
       '3154', '3194', '3195', '3199', '3217', '3220', '323', '3240',
       '3244', '3247', '3248', '325', '3259', '3273', '3293', '3330',
       '3336', '3368', '3371', '3400', '3432', '3448', '3457', '3461',
       '3473', '3474', '3483', '3515', '3542', '3560', '3586', '3601',
       '3608', '3620', '3631', '3647', '3668', '367', '3711', '3753',
       '38', '3823', '3824', '3826', '3843', '3845', '3848', '3901',
       '3907', '3912', '3921', '3926', '3957', '3978', '3982', '3985',
       '3996', '4025', '4038', '4040', '4042', '4073', '4075', '4076',
       '4088', '4108', '4111', '4123', '4131', '4133', '4156', '4166',
       '4190', '4201', '4205', '421', '4218', '4252', '4256', '4265',
       '4272', '4287', '429', '4296', '4328', '4335', '4351', '4375',
       '4379', '438', '4383', '4390', '4398', '4399', '443', '4438',
       '4440', '4447', '4448', '4455', '4469', '4478', '4502', '451',
       '4530', '4536', '4539', '4543', '4551', '4552', '4556', '4566',
       '4574', '4576', '4579', '4580', '4591', '4603', '4616', '4640',
       '466', '4667', '4673', '4702', '4704', '4711', '4724', '4734',
       '475', '4751', '4760', '4761', '4764', '4767', '4783', '4799',
       '480', '4802', '4851', '4862', '4864', '4868', '4872', '4882',
       '4888', '4889', '4908', '4921', '4932', '4954', '4962', '4963',
       '4973', '4978', '5005', '5012', '5028', '5036', '5039', '504',
       '5073', '5075', '5103', '5111', '5112', '5118', '5135', '5140',
       '5151', '5152', '5183', '5198', '5212', '5229', '5230', '5246',
       '5249', '5265', '5284', '5315', '532', '5337', '5342', '5347',
       '5351', '5354', '5366', '5374', '5385', '540', '5456', '5462',
       '5467', '5472', '5484', '5490', '5492', '5500', '5518', '5534',
       '5535', '5537', '5545', '5549', '5559', '556', '5571', '5586',
       '5596', '5598', '5627', '5635', '5644', '5658', '5677', '57',
       '5724', '5739', '5758', '5783', '5789', '5807', '5808', '5811',
       '5818', '5823', '5848', '5852', '587', '5890', '5894', '5908',
       '5919', '5927', '598', '602', '605', '606', '610', '622', '639',
       '643', '650', '661', '681', '684', '685', '686', '695', '702',
       '71', '713', '714', '759', '764', '765', '798', '803', '805',
       '812', '843', '847', '850', '851', '869', '875', '903', '909',
       '92', '921', '926', '927', '932', '948', '956', '970', '971',
       '973', '975', '976', '982', '997']

validationRegions = ['1209', '1321', '1500', '1638', '1806', '1807', '1907', '1930',
       '1993', '1999', '2372', '2491', '2519', '2546', '2635', '2636',
       '2673', '2716', '2809', '3311', '3344', '3520', '3580', '3730',
       '384', '3877', '3894', '392', '393', '3941', '3999', '4344',
       '4396', '4817', '4941', '5127', '5186', '5541', '5692', '5738',
       '5745', '637', '750', '8', '833', '899', '902', '940', '1038', '1046', '107', '1089', '1126', '1133', '116', '1168',
       '1171', '1183', '1271', '1345', '1353', '1389', '1410', '145',
       '146', '1471', '1483', '1492', '1527', '1549', '1557', '1558',
       '1574', '1611', '1613', '1658', '1669', '1672', '1697', '1701',
       '1744', '175', '1756', '1845', '185', '1877', '1893', '190',
       '1949', '1959', '1962', '198', '2026', '2037', '2110', '2117',
       '2121', '2123', '2131', '2143', '2166', '2173', '2178', '2203',
       '2270', '2358', '2366', '2400', '2420', '2502', '2522', '2533',
       '2543', '2560', '2598', '2599', '2605', '2619', '2663', '2711',
       '2735', '2825', '2861', '2875', '2922', '2945', '2954', '2955',
       '2999', '3028', '3103', '318', '3205', '3246', '3252', '3267',
       '327', '3286', '3288', '3309', '3323', '3326', '3415', '3420',
       '3481', '3490', '3513', '355', '3604', '3635', '364', '3648',
       '3700', '3703', '3719', '3741', '3785', '3821', '3874', '3942',
       '3965', '3974', '4011', '403', '4065', '4092', '4093', '414',
       '4228', '4284', '4288', '43', '4315', '4321', '4337', '4397',
       '4424', '444', '4454', '4466', '4477', '4505', '4523', '4541',
       '4549', '4559', '46', '4610', '4623', '4655', '4661', '4678',
       '4718', '4726', '4792', '4800', '4900', '4942', '4943', '4969',
       '4991', '4995', '5002', '5004', '5022', '5051', '5054', '51',
       '5113', '5163', '5208', '5275', '5293', '5355', '5375', '538',
       '5387', '5413', '5422', '5521', '5543', '5544', '5550', '5577',
       '5618', '5678', '5710', '5718', '5750', '576', '5772', '580',
       '5820', '5831', '5856', '5865', '5880', '5916', '618', '625',
       '640', '652', '662', '674', '705', '712', '725', '740', '794',
       '824', '853', '854', '86', '867', '900', '913', '918', '925',
       '950', '986']

train = []
validation = []

print('creating the train files....')

for trainRegion in trainRegions:
    paths = '../shared/Data/HMI_LOS_SHARPS/valid_magnetograms/los/%s_*.dat' %trainRegion
    filePaths = glob.glob(paths)
    train.extend(filePaths)

img = np.load(train[0])
print(img.shape)

print('creating the train files....')
for validationRegion in validationRegions:
    paths = '../shared/Data/HMI_LOS_SHARPS/valid_magnetograms/los/%s_*.dat' %validationRegion
    filePaths = glob.glob(paths)
    validation.extend(filePaths)


np.random.shuffle(train)
np.random.shuffle(validation)

TRAIN = {}
VALIDATION = {}

TRAIN[str(0)] = train
VALIDATION[str(0)] = validation


print('creating allPaths.....')
allPaths = train + validation





print('calculating mean and std deviation....')

mean = 0.0

for i in range(len(allPaths)):
    image = np.load(allPaths[i])
    mean += np.mean(image)
    
mean = mean / len(allPaths)

sum = 0.0
N = 256*256*len(allPaths)

for F in allPaths:
    image = np.load(F)
    sum += ((image - mean)**2).sum()

std = np.sqrt(sum/N)
"""

TRAIN = np.load('rasha_train.npy')
VALIDATION = np.load('rasha_validation.npy')

#train_mean_crossval = np.load("train_mean_crossval.npy")
#validation_mean_crossval = np.load("validation_mean_crossval.npy")
#train_std_crossval = np.load("train_std_crossval.npy")
#validation_std_crossval = np.load("validation_std_crossval.npy")

IMAGE_SIZE = 256

CHANNELS = 1

#n_of_train_samples = len(TRAIN.get('0'))
#n_of_val_samples = len(VALIDATION.get('0'))

n_of_train_samples = len(TRAIN.item().get('0'))
n_of_val_samples = len(VALIDATION.item().get('0'))

global GLOB_TSS
global LOSSES
global STATS

def get_y_val(file):
    return float(os.path.split(file)[-1][-5])
    
def get_test_data(j):
    x_data = []
    y_data = []
    for f in VALIDATION.item().get(str(j)):
        this_x = np.load(f)
        this_x = this_x.reshape(IMAGE_SIZE, IMAGE_SIZE, CHANNELS)
        #this_x = ((this_x - mean) / std)
        x_data.append(this_x)
        y_data.append(get_y_val(f))
    x_data = np.array(x_data, dtype = np.float32)
    y_data = np.array(y_data, dtype = np.float32)
    return (x_data, y_data)

def calc_TSS(confmat, nClasses):
    stats = {"recall": np.array([]), "Accuracy": np.array([]), "TSS": np.array([])}
    for i in xrange(0, nClasses):
        tp = confmat[i,i]
        tn = 0.0
        for j in range (i+1,nClasses):
            tn += confmat[j,j]
        for j in range (0,i):
            tn += confmat[j,j]
        fp = confmat[:,i].sum() - tp
        fn = confmat[i,:].sum() - tp
        myscore = float(( tp + tn )) / ( tp + tn + fp + fn)
        stats["Accuracy"] = np.append(stats["Accuracy"], myscore)
        myscore = float( tp ) / (tp + fn )
        stats["recall"] = np.append(stats["recall"], myscore)
        myscore = float( tp ) / ( tp + fn ) - float( fp ) / ( fp + tn )
        stats["TSS"] = np.append(stats["TSS"], myscore)
    STATS.append(stats)
    GLOB_TSS.append(stats['TSS'][0])

cache = {}
def generator(j):
    data_x = []
    data_y = []
    i = 0
    while True:
        FILE = TRAIN.item().get(str(j))[i]

        if FILE in cache:
            data_x_sample = cache[FILE][0]
            data_y_sample = cache[FILE][1]
        else:
            shape = (IMAGE_SIZE, IMAGE_SIZE, CHANNELS)
            data_x_sample = np.load(FILE)
            #data_x_sample = ((data_x_sample - mean) / std)
            data_y_sample = get_y_val(FILE)

            if psutil.virtual_memory().percent < 85:
                cache[FILE] = [data_x_sample, data_y_sample]

        data_x.append(data_x_sample)
        data_y.append(data_y_sample)
        i += 1
        if i == len(TRAIN.item().get(str(j))):
            i = 0
            np.random.shuffle(TRAIN.item().get(str(j)))

        if BATCH_SIZE == len(data_x):
            ret_x = np.reshape(data_x, (len(data_x), IMAGE_SIZE, IMAGE_SIZE, CHANNELS))
            ret_y = np.reshape(data_y, (len(data_y), CHANNELS))
            yield (ret_x, ret_y)
            data_x = []
            data_y = []

def validation_generator(j):
    data_x = []
    data_y = []
    i = 0
    while True: 

        FILE = VALIDATION.item().get(str(j))[i]

        if FILE in cache:
            data_x_sample = cache[FILE][0]
            data_y_sample = cache[FILE][1]
        else:
            shape = (IMAGE_SIZE, IMAGE_SIZE, CHANNELS)
            data_x_sample = np.load(FILE)
            #data_x_sample = ((data_x_sample - mean) / std)
            data_y_sample = get_y_val(FILE)
            
            if psutil.virtual_memory().percent < 85:
                cache[FILE] = [data_x_sample, data_y_sample]

        data_x.append(data_x_sample)
        data_y.append(data_y_sample)
        i += 1
        if i == len(VALIDATION.item().get(str(j))):
            i = 0
            np.random.shuffle(VALIDATION.item().get(str(j)))

        if BATCH_SIZE == len(data_x):
            ret_x = np.reshape(data_x, (len(data_x), IMAGE_SIZE, IMAGE_SIZE, CHANNELS))
            ret_y = np.reshape(data_y, (len(data_y), CHANNELS))
            yield (ret_x, ret_y)
            data_x = []
            data_y = []    


class simplecallback(keras.callbacks.Callback):

    def __init__(self, j):
        self.logpath = os.getcwd()
        self.j = j

    def on_train_begin(self, logs={}):
        self.losses = [[], []]
        return

    def on_epoch_end(self, epoch, logs={}):
        self.losses[0].append(logs.get("loss"))
        self.losses[1].append(logs.get("val_loss"))
        
    def on_train_end(self, logs={}):
        """
        num = np.array([], dtype = np.int8)
        search_path = 'cnn_without_padding/%s/losses_*.npy' %(name)
        files = glob.glob(search_path)
        if files == []:
            N = 1
        else:
            for f in files:
                n = int(os.path.split(f)[-1][-5])
                num = np.append(num, n)
            N = np.max(num) + 1
        write_path = 'cnn_without_padding/%s/losses_%s.npy' %(name, N) 
        np.save(write_path, self.losses)
        """
        LOSSES.append(self.losses)
        print(self.j)
        (x_test, y_test) = get_test_data(self.j)
        y_pred = model.predict(x_test)
        y_pred = y_pred.squeeze()
        print(y_pred)
        y_pred[y_pred < 0.5] = 0
        y_pred[y_pred >= 0.5] = 1
        print(y_pred)

        y_test = y_test.squeeze()
        print(y_test)
        confmat = confusion_matrix(y_test,y_pred)
        print(confmat)
        calc_TSS(confmat,2)


FILTERS = {
    'F1': (3,3),
    'F2': (5,5),
    'F3': (7,7)
}

dropout_val = 0.4

num_layer = 2

def InceptionModule(A_prev):
    X1 = Conv2D(8, FILTERS['F1'], strides = (1,1), padding = 'same')(A_prev)
    #X1 = Activation('relu')(X1)
    X1 = LeakyReLU(alpha=0.1)(X1)

    X2 = Conv2D(8, FILTERS['F2'], strides = (1,1), padding = 'same')(A_prev)
    #X2 = Activation('relu')(X2)
    X2 = LeakyReLU(alpha=0.1)(X2)

    X3 = Conv2D(8, FILTERS['F3'], strides = (1,1), padding = 'same')(A_prev)
    #X3 = Activation('relu')(X3)
    X3 = LeakyReLU(alpha=0.1)(X3)

    X4 = MaxPooling2D((5,5), strides = (1,1), padding='same')(A_prev)
    X4 = Conv2D(8, (1,1), strides = (1,1))(X4)
    #X4 = Activation('relu')(X4)
    X4 = LeakyReLU(alpha=0.1)(X4)

    X = concatenate([X1, X2, X3, X4], axis = 3)

    return X


def InceptionModel(input_shape, classes):
    X_input = Input(input_shape)
    X = Conv2D(8, (7,7), strides = (2,2))(X_input)
    X = BatchNormalization(axis = 3)(X)
    #X = BatchNormalization(axis = 3)(X)
    X = LeakyReLU(alpha=0.1)(X)
    #X = Activation('relu')(X)
    X = MaxPooling2D(pool_size = (3,3), strides = (2,2))(X)
    X = Conv2D(16, (3,3), strides = (1,1))(X)
    #X = BatchNormalization(axis = 3)(X)
    X = LeakyReLU(alpha=0.1)(X)
    #X = Activation('relu')(X)
    X = MaxPooling2D(pool_size = (3,3), strides = (2,2))(X)
    for i in range(num_layer):
        X = InceptionModule(X)
        if (i == num_layer - 1):
            X = GlobalAveragePooling2D()(X)
        else:
            X = MaxPooling2D(pool_size = (3,3), strides = (2,2))(X)
    #X = InceptionModule(X)
    #X = MaxPooling2D(pool_size = (3,3), strides = (2,2))(X)
    #X = InceptionModule(X)
    #X = GlobalAveragePooling2D()(X)
    X = Dropout(dropout_val)(X)
    X = Dense(classes, activation = 'sigmoid')(X)

    model = Model(inputs = X_input, outputs = X)
    return model


BATCH_SIZE = 64

class_weight = {0: 1.,
                1: 4.5}
cross_val = 1

GLOB_TSS = []
LOSSES = []
STATS = []

learning = 0.000005
EPOCHS = 2

layer_number = 2

for i in range(cross_val):
    model = InceptionModel((IMAGE_SIZE, IMAGE_SIZE, CHANNELS), 1)

    print(model.summary())
    
    model.compile(
    loss='binary_crossentropy',
    optimizer=optimizers.Adam(lr = learning),
    metrics=['acc'])
    
    model.fit_generator(
    generator(i),
    steps_per_epoch=n_of_train_samples//BATCH_SIZE,
    validation_data=validation_generator(i),
    validation_steps=n_of_val_samples//BATCH_SIZE,
    epochs=EPOCHS,
    class_weight=class_weight,
    callbacks=[simplecallback(i)])

print(GLOB_TSS)

num = np.array([], dtype = np.int8)
search_path = 'inception_with_generator/rasha_cross_vals_*.npy'
files = glob.glob(search_path)
if files == []:
    N = 1
else:
    for f in files:
        n = int(f.split('_')[-1].split('.')[0])
        #n = int(os.path.split(f)[-1][11:-4])
        num = np.append(num, n)
    N = np.max(num) + 1

parameters = {'lr': learning, 'batch size': BATCH_SIZE, 'number of training files': n_of_train_samples, 'number of validation files': n_of_val_samples, 'epochs': EPOCHS, 'class_weight': class_weight, 'cross validation': cross_val, 'inception_filters': FILTERS, 'dropout': dropout_val, 'batch_norm': 'no', 'inception_layers': num_layer, 'activation': 'leakyrelu'}


write_path = 'inception_with_generator/rasha_params_%s.npy' %(N) 
np.save(write_path, parameters)

write_path = 'inception_with_generator/rasha_cross_vals_%s.npy' %(N) 
np.save(write_path, GLOB_TSS)

CROSS_VAL = np.mean(GLOB_TSS)
write_path = 'inception_with_generator/rasha_cross_val_%s.npy' %(N) 
np.save(write_path, CROSS_VAL)

model_write_path = 'inception_with_generator/rasha_model_%s.h5' %(N)
model.save(model_write_path)

write_path = 'inception_with_generator/rasha_losses_%s.npy' %(N) 
np.save(write_path, LOSSES)

write_path = 'inception_with_generator/rasha_stats_%s.npy' %(N) 
np.save(write_path, STATS)

load_path = 'inception_with_generator/rasha_cross_val_%s.npy' %(N)
tss = np.load(load_path)
print('cross validation TSS : ', tss)

dev = np.std(GLOB_TSS)
print(dev)